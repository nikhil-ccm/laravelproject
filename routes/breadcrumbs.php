<?php

// Home
Breadcrumbs::for('home', function ($trail) {
    $trail->push('Home', url('/'));
});

Breadcrumbs::for('contacts' ,function($trail) {
    $trail->parent('home');
    $trail->push('Contacts' , route('contacts.index'));
});

Breadcrumbs::for('edit' ,function($trail) {
    $trail->parent('contacts');
    $trail->push('Edit' , url('contacts/{id}/edit'));
});

//Login
Breadcrumbs::for('login' ,function($trail) {
    $trail->parent('home');
    $trail->push('Login' , route('login'));
});

//Register
Breadcrumbs::for('register' ,function($trail) {
    $trail->parent('home');
    $trail->push('Register' , route('register'));
});

//Catagory
Breadcrumbs::for('catagory' ,function($trail) {
    $trail->parent('home');
    $trail->push('Catagory' , route('catagories.index'));
});

//CtagoryEdit
Breadcrumbs::for('catagoryEdit' ,function($trail) {
    $trail->parent('catagory');
    $trail->push('Edit' , url('catagories/{id}/edit'));
});

/*
// Home > About
Breadcrumbs::for('about', function ($trail) {
    $trail->parent('home');
    $trail->push('About', route('about'));
});

// Home > Blog
Breadcrumbs::for('blog', function ($trail) {
    $trail->parent('home');
    $trail->push('Blog', route('blog'));
});

// Home > Blog > [Category]
Breadcrumbs::for('category', function ($trail, $category) {
    $trail->parent('blog');
    $trail->push($category->title, route('category', $category->id));
});

// Home > Blog > [Category] > [Post]
Breadcrumbs::for('post', function ($trail, $post) {
    $trail->parent('category', $post->category);
    $trail->push($post->title, route('post', $post->id));
});
*/