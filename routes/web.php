<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { return view('welcome'); });

//Route::get('/', function () { return view('custom'); });

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/mypage','pageController@index');
Route::get('/view-profile', 'HomeController@viewProfile');
Route::get('{id}/edit', 'HomeController@edit');
Route::post('/update-profile', 'HomeController@updateProfile');

Route::get('/pages/pushstack', function() { return view('pages.pushstack'); });

//Custom Password reset routes
/*Route::get('/password/confirm' , 'CustomAuth\ConfirmPasswordController@showConfirmForm')->name('password.confirm');
Route::post('/password/confirm' , 'CustomAuth\ConfirmPasswordController@confirm');
Route::post('/password/email' , 'CustomAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('custom-auth/password/reset' , 'CustomAuth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('custom-auth/password/reset' , 'CustomAuth\ResetPasswordController@reset')->name('password.update');
Route::get('/password/reset/{token?}' , 'CustomAuth\ResetPasswordController@showResetForm')->name('password.reset');*/


Route::get('/custom-auth/login' , 'CustomAuth\LoginController@index')->name('custom-auth/login');
Route::post('/custom-auth/login' , 'CustomAuth\LoginController@login');
Route::get('/custom-auth/logout' , 'CustomAuth\LoginController@logout');
Route::get('/custom-auth/forgot-password' , 'CustomAuth\ForgotPasswordController@index');

Route::get('/CustomHome' , 'CustomHomeController@index');
Route::get('/edit-profile/{id}' , 'CustomAuth\EditProfileController@index');
Route::post('/edit-profile/{id}' , 'CustomAuth\EditProfileController@updateProfile');

Route::get('/custom-auth/register' , 'CustomAuth\RegisterController@index')->name('custom-auth/register');
Route::post('custom-auth/register' , 'CustomAuth\RegisterController@register');

Route::get('/pages/dataTable',function(){ return view('pages.dataTable');});


Route::resource('contacts' , 'ContactController');
Route::resource('catagories' , 'CatagoryController');