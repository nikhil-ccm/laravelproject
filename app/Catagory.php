<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\user;
class Catagory extends Model
{
    public $timestamps = false;
    protected $table= 'catagories';
    protected $fillable = ['user_id','name'];
    public function user(){
        return $this->belongsTo('App\User');
    }
}
