<?php

namespace App\Http\Controllers\CustomAuth;

use App\User;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class RegisterController extends Controller
{
    public function index() {
        return view ('custom-auth.register');
    }

    public function register(Request $request)
    {
        $validate=null;
        $validate=$request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed' , 'regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\X])(?=.*[!$#@%]).*$/']
        ]);
        $request['password'] = bcrypt($request->password);
        if($validate)
        {
            $user = User::create($request->all());
            return redirect('custom-auth/login')->with('status' , 'account created successfully');
        }

    }
}
