<?php

namespace App\Http\Controllers\CustomAuth;

use App\User;
use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{


    public function index() {
        if(Auth::user()) {
            return redirect('CustomHome');
        } else {
            return view ('custom-auth.login');
        }
    }

    public function login(Request $request)
    {
        $this->validate($request , [
            'email' => 'required|email'
        ]);
        
        if(Auth::attempt(['email' => $request->email , 'password' => $request->password]))
        {
            $user = Auth::user()->id;
            return redirect('CustomHome')->withUser($user);
        } else {
            return redirect ('custom-auth/login')->with('error', 'invalid creditionals');
        }
    }

    public function logout() {
        Auth::logout();
        return redirect('/');
    }
    
}