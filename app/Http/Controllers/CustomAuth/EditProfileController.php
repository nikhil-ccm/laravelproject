<?php

namespace App\Http\Controllers\CustomAuth;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EditProfileController extends Controller
{
    public function index(){
        return view('custom-auth.edit-profile');
    }

    public function updateProfile(Request $request) {
        $validate = null;
        $user = User::find(auth()->user()->id);
        if($user) {
            if(Auth::user()->email === $request->email) { 
                
                $validate = $request->validate([
                    'name' => 'required|min:2',
                    'email' => 'required|email'
                ]);    

            } else {
             $validate = $request->validate([
                  'name' => 'required|min:2',
                  'email' => 'required|email|unique:users'
                ]);
             }
          if($validate) {
            $user->name = $request->input('name');
            $user->email =$request->input('email');

            $user->save();
            return redirect('CustomHome')->with('success' , 'data updated');
          } else {
              return redirect()->back();
          }
}
}
}
