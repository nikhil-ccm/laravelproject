<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Catagory;
use Illuminate\Support\Facades\Auth;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user_id = auth()->user()->id;
        $user = User::find($user_id);
        return view('home')->with('catagories' , $user->catagories);
    }

    public function viewProfile() {
        if(Auth::user()) {
            $user = User::find(auth()->user()->id);
        if($user) {
            return view('/auth.view-profile')->withUser($user);
        } else {
            return redirect()->back();
        }
        } else {
            return redirect()->back();
        }
        
    }

    public function edit() {
        if(Auth::user()) {
            $user = User::find(auth()->user()->id);
        if($user) {
            return view('/auth.edit')->withUser($user);
        } else {
            return redirect()->back();
        }
        } else {
            return redirect()->back();
        }
    }

    public function updateProfile(Request $request) {
            $validate = null;
            $user = User::find(auth()->user()->id);
            if($user) {
                if(Auth::user()->email === $request->email) { 
                    
                    $validate = $request->validate([
                        'name' => 'required|min:2',
                        'email' => 'required|email'
                    ]);    

                } else {
                 $validate = $request->validate([
                      'name' => 'required|min:2',
                      'email' => 'required|email|unique:users'
                    ]);
                 }
              if($validate) {
                $user->name = $request->input('name');
                $user->email =$request->input('email');

                $user->save();
                return redirect('auth/view-profile')->with('success' , 'data updated');
              } else {
                  return redirect()->back();
              }
    }
}
}