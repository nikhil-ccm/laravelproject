@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                        <div class="panel-body">
                            <a href="{{ route('catagories.create') }}" class="btn btn-primary">Create Ctagory</a>
                            <h3>Your Catagories</h3>
                            @if(count($catagories) > 0)
                            <table class="table table-striped">
                                <tr>
                                    <th>Nmae</th>
                                    <th colspan="2">Acions</th>
                                </tr>
                                @foreach($catagories as $userCatagory)
                                <tr>
                                    <th>{{ $userCatagory->name }}</th>
                                    <th><a href="{{ url('catagories/'.$userCatagory->user_id.'/edit') }}" class="btn btn-primary">Edit</a></th>
                                    <th><a href="#" class="btn btn-danger">Delete</a></th>
                                </tr>
                                @endforeach
                            </table>
                            @else
                            <p>you have no catagories yet</p>
                            @endif
                        </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
