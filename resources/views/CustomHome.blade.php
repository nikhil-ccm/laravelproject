<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    <script src="{{ asset('js/jquery.js') }}"></script>
    <script src="{{ asset('js/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('js/additional-methods.min.js') }} "></script>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div class="container">
    @if(Route::has('custom-auth/login'))
    <div class="top-right links">
        @if(Auth::check())
            <a href="{{ url('/') }}">Home</a>
        <a href="{{ url('custom-auth/logout') }}">Logout
     </a>
       
        @else
            <a href="{{ route('custom-auth/login') }}">Login</a>

            <a href="{{ route('custom-auth/register') }}">Register</a>
        @endif
    </div>
    @endif
    <div class="row">
        <div class="col-sm-12">
            @if(session('success'))
                <div class="alert" role="alert">
                    {{session('success')}}
                </div>
            @endif
        <h1 class="text-primary">Name is {{Auth::user()->name}}</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
        <h1 class="text-primary">Email is {{Auth::user()->email}}</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
        <a href="CustomHome" class="btn btn-primary">View Profile</a>
        <a href="edit-profile/{{Auth::user()->id}}" class="btn btn-success">Update Profile</a>
        </div>
    </div>
</div>

</body>
</html>