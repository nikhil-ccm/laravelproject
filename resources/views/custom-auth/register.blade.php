<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div class="container">
        @if(Route::has('custom-auth/login'))
        <div class="top-right links">
            @if(Auth::check())
                <a href="{{ url('/home') }}">Home</a>
                <a href="{{ url('/CustomHome') }}">Dashboard</a>
                <a href="{{ url('custom-auth/logout') }}">Logout
         </a>
           
            @else
                <a href="{{ route('custom-auth/login') }}">Login</a>

                <a href="{{ route('custom-auth/register') }}">Register</a>
            @endif
        </div>
        @endif
        <br><br>
        <div class="row">
            <div class="col-sm-12">
                <h1 class="text-primary text-center">Register</h1>
            </div>
        </div>

        @if(count($errors) > 0)
            @foreach($errors->all() as $error)
                <p class="alert alert-danger">
                    {{$error}}
                </p>
             @endforeach
         @endif
         
        {!! Form::open(['route' => 'custom-auth/register' , 'method' => 'POST' , 'id' => 'registerForm']) !!}
            {{Form::token()}}
                    
            <div class="form-group">
                {{Form::label('name' , 'enter name')}}
                {{Form::text('name', '', ['class' => 'form-control' , 'placeholder' => 'enter name' , 'id' => 'name'])}}
            </div>

            <div class="form-group">
                {{Form::label('email','email address')}}
                {{Form::email('email', '', ['class' => 'form-control' , 'placeholder' => 'enter email' , 'id' => 'email'])}}
            </div>
        
            <div class="form-group">
                {{Form::label('password','password')}}
                {{Form::password('password', ['class' => 'form-control' , 'id' => 'password'])}}
            </div>
        
            <div class="form-group">
                {{Form::label('password_confirmation', 'enter password again' )}}
                {{Form::password('password_confirmation' , ['id' => 'password_confirmation', 'class' => 'form-control'])}}
            </div>
                    
            <div class="form-group">
                {{Form::submit('Register',['class' => 'btn btn-primary'])}}
            </div>
        {!! Form::close() !!}
        
    </div>

</body>
</html>