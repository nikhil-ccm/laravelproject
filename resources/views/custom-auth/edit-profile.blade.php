<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div class="container">

    @if(count($errors) > 0)
    @foreach($errors->all() as $error)
        <p class="alert alert-danger">
            {{$error}}
        </p>
    @endforeach
    @endif
    {!! Form::open(['url' => '/edit-profile/{id}' , 'method' => 'POST' , 'id' => 'updateForm']) !!}
    
    {{Form::token()}}
    
    <div class="form-group">
        {{Form::label('name','name')}}
        {{Form::text('name', Auth::user()->name, ['class' => 'form-control' , 'placeholder' => 'enter name' ,'id' => 'name' , 'minlength' => 2 , 'maxlength' => 20 , 'required'])}}
    </div>

    <div class="form-group">
        {{Form::label('email','email address')}}
        {{Form::email('email', Auth::user()->email, ['class' => 'form-control' , 'placeholder' => 'enter email' , 'id' => 'email' , 'email' => 'true' , 'required'])}}
    </div>

    <div class="form-group">
        {{Form::submit('Update',['class' => 'btn btn-secondary'])}}
    </div>
    {!! Form::close() !!}
</div>
<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>