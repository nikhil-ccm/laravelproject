<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div class="container">
        @if(Route::has('custom-auth/login'))
        <div class="top-right links">
            @if(Auth::check())
                <a href="{{ url('/home') }}">Home</a>
                <a href="{{ url('/CustomHome') }}">Dashboard</a>
                <a href="{{ url('custom-auth/logout') }}">Logout
         </a>
           
            @else
                <a href="{{ route('custom-auth/login') }}">Login</a>

                <a href="{{ route('custom-auth/register') }}">Register</a>
            @endif
        </div>
        @endif
        <br><br>
        <div class="row">
            <div class="col-sm-12">
                <h1 class="text-primary text-center">Login</h1>
            </div>
        </div>

        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif


        @if(count($errors) > 0)
            @foreach($errors->all() as $error)
                <p class="alert alert-danger">
                    {{$error}}
                </p>
             @endforeach
         @endif
         
        {!! Form::open(['route' => 'custom-auth/login' , 'method' => 'post' , 'id' => 'loginForm']) !!}
            {{Form::token()}}
                    
            <div class="form-group">
                    {{Form::label('email','email address')}}
                    {{Form::email('email', old('email'), ['class' => 'form-control' , 'placeholder' => 'enter email' , 'id' => 'email'])}}
            </div>
        
            <div class="form-group">
                {{Form::label('password','password')}}
                {{Form::password('password', ['class' => 'form-control /*is-invalid*/' , 'id' => 'password'])}}
            </div>

            @if (session('error'))
                <div class="alert alert-success" role="alert">
                    {{ session('error') }}
                </div>
            @endif
        
            <div class="form-check">
                    {{Form::label('remember', 'Remember Me!' , ['class' => 'form-check-label' , 'id' => 'remember'] )}}
                    {{Form::checkbox('remember' , '', ['id' => 'remember', 'class' => 'form-check-input'])}}
            </div>

            <div class="form-group">
                {{Form::submit('Login',['class' => 'btn btn-primary'])}}
            </div>
        
            <div class="form-group">
            <a class="btn btn-link" href="{{ route('password.request') }}">
                         {{ __('Forgot Your Password?') }}
                    </a>
            </div>
        {!! Form::close() !!}
        
    </div>
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>