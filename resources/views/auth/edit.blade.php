@extends('layouts.app')

@section('content')
<div class="container">

    @if(count($errors) > 0)
    @foreach($errors->all() as $error)
        <p class="alert alert-danger">
            {{$error}}
        </p>
    @endforeach
    @endif
    {!! Form::open(['action' => 'HomeController@updateProfile' , 'method' => 'POST' , 'id' => 'updateForm']) !!}
    
    {{Form::token()}}
    
    <div class="form-group">
        {{Form::label('name','name')}}
        {{Form::text('name', $user->name, ['class' => 'form-control' , 'placeholder' => 'enter name' ,'id' => 'name' , 'minlength' => 2 , 'maxlength' => 20 , 'required'])}}
    </div>

    <div class="form-group">
        {{Form::label('email','email address')}}
        {{Form::email('email', $user->email, ['class' => 'form-control' , 'placeholder' => 'enter email' , 'id' => 'email' , 'email' => 'true' , 'required'])}}
    </div>

    <div class="form-group">
        {{Form::submit('Update',['class' => 'btn btn-secondary'])}}
    </div>
    {!! Form::close() !!}
</div>
@endsection