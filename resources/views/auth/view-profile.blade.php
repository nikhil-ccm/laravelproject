@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            @if(session('success'))
                <div class="alert" role="alert">
                    {{session('success')}}
                </div>
            @endif
        <h1 class="text-primary">Name is {{$user->name}}</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
        <h1 class="text-primary">Email is {{$user->email}}</h1>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
        <a href="view-profile" class="btn btn-primary">View Profile</a>
        <a href="{{$user->id}}/edit" class="btn btn-success">Update Profile</a>
        </div>
    </div>
</div>
@endsection