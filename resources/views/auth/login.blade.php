@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if(count($errors) > 0)
                @foreach($errors->all() as $error)
                    <p class="alert alert-danger">
                        {{$error}}
                    </p>
                @endforeach
            @endif
            {{ Breadcrumbs::render('login') }}
         {!! Form::open(['route' => 'login' , 'method' => 'POST' , 'id' => 'loginForm']) !!}
            {{Form::token()}}
            
            <div class="form-group">
                {{Form::label('email','email address')}}
                {{Form::email('email', '', ['class' => 'form-control' , 'placeholder' => 'enter email' , 'id' => 'email' , 'email' => 'true' ,'required'])}}
            </div>

            <div class="form-group">
                {{Form::label('password','password')}}
                {{Form::password('password', ['class' => 'form-control', 'id' => 'password' ,'required'])}}
            </div>

            <div class="form-check">
                    {{Form::label('remember', 'Remember Me!' , ['class' => 'form-check-label' , 'id' => 'remember'] )}}
                    {{Form::checkbox('remember' , '', ['id' => 'remember', 'class' => 'form-check-input'])}}
            </div>
            
            <div class="form-group">
                {{Form::submit('Login',['class' => 'btn btn-primary'])}}
            </div>

            <div class="form-group">
                @if (Route::has('password.request'))
                     <a class="btn btn-link" href="{{ route('password.request') }}">
                         {{ __('Forgot Your Password?') }}
                          </a>
                @endif
            </div>
            {!! Form::close() !!}

        

           <!--
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        -->
        </div>
    </div>
</div>
@endsection