@extends('catagoryBase')

@section('main')
<div class="row">
 <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Create Catagory</h1>
  <div>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      
    {!!Form::open(['route' => 'catagories.store' , 'method' => 'post'])!!}
    {{Form::token()}}
  <div class="form-group">
        {{Form::label('name' , 'Enter Catagory Name:')}}
        {{Form::text('name' , null , ['class' =>'form-control' , 'id' => 'name' , 'minlength' => 2 , 'maxlength' => 20])}}
    </div>

    <div class="form-group">
        {{Form::submit('Add Catagory' , ['class' =>'btn btn-primary'])}}
    </div>
    {!!Form::close()!!}

  </div>
</div>
</div>
@endsection
