@extends('catagoryBase')
@section('main')

<div class="col-sm-12">

    @if(session()->get('success'))
      <div class="alert alert-success">
        {{ session()->get('success') }}  
      </div>
    @endif

    @if(session()->get('error'))
    <div class="alert alert-danger">
      {{ session()->get('error') }}  
    </div>
    @endif
    
 
    
<div class="row">
<div class="col-sm-12">
    <h1 class="display-3">Catagories</h1> 
    <div>
        <a style="margin: 19px;" href="{{ route('catagories.create')}}" class="btn btn-primary">New catagory</a>
    </div>

    {{ Breadcrumbs::render('catagory') }}

  <table id="myTable" class="display">
    <thead>
        <tr>
          <td>User Id</td>
          <td>Name</td>
        </tr>
    </thead>
    <tbody>
      @foreach($catagoryUser as $user)
      <tr>
          <td>{{$user->user_id}}</td>
          <td>{{$user->name}}</td>
          <td>
              <a href="{{ route('catagories.edit',$user->user_id)}}" class="btn btn-primary btn-sm">Edit</a>

            <button class="btn btn-danger btn-sm">Delete</button> 
            <!--<form action="{{ route('catagories.destroy',$user->user_id)}}" method="post">
                @csrf
                @method('DELETE')
                <button class="btn btn-danger" type="submit">Delete</button>
              </form>-->
          </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  <script>
    $(document).ready( function () {
     $('#myTable').DataTable();
 });
</script>
@endsection
