<!DOCTYPE html>
<html lang="en">
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Catagory</title>
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    
  <script src="{{ asset('js/app.js') }}" ></script>

  
</head>
<body>
  <div class="container">
    @include('sweet::alert')
    @yield('main')
  </div>
</body>
</html>