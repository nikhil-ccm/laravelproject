@extends('base') 
@section('main')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h1 class="display-3">Update a contact</h1>
        
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <br /> 
        @endif
        
        {{ Breadcrumbs::render('edit') }}

        {!!Form::open(['route' => ['contacts.update' , $contact->id ] , 'method' => 'PATCH'])!!}
        {{Form::token()}}
        <div class="form-group">
            {{Form::label('first_name' , 'First Name:')}}
            {{Form::text('first_name' , $contact->first_name , ['class' =>'form-control' , 'id' => 'first_name' , 'minlength' => 2 , 'maxlength' => 20 , 'required' => 'true'])}}
        </div>
    
        <div class="form-group">
            {{Form::label('last_name' , 'Last Name:')}}
            {{Form::text('last_name' , $contact->last_name , ['class' =>'form-control' , 'id' => 'last_name' , 'minlength' => 2 , 'maxlength' => 20 , 'required' => 'true'])}}
        </div>
    
        <div class="form-group">
            {{Form::label('email' , 'Email:')}}
            {{Form::email('email' , $contact->email , ['class' =>'form-control' , 'id' => 'email' , 'email' => 'true' , 'required' => 'true'])}}
        </div>
    
        <div class="form-group">
            {{Form::label('city' , 'City:')}}
            {{Form::text('city' , $contact->city , ['class' =>'form-control' , 'id' => 'city' , 'required' => 'true'])}}
        </div>
    
        <div class="form-group">
            {{Form::label('country' , 'Country:')}}
            {{Form::text('country' , $contact->country , ['class' =>'form-control' , 'id' => 'country' , 'required' => 'true'])}}
        </div>
    
        <div class="form-group">
            {{Form::label('job_title' , 'Job Title:')}}
            {{Form::text('job_title' , $contact->job_title , ['class' =>'form-control' , 'id' => 'job_title'])}}
        </div>
    
        <div class="form-group">
            {{Form::submit('Update' , ['class' =>'btn btn-primary'])}}
        </div>
        {!!Form::close()!!}
        
    </div>
</div>
@endsection