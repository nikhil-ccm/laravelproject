@extends('base')

@section('main')
<div class="row">
 <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Add a contact</h1>
  <div>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      
    {!!Form::open(['route' => 'contacts.store' , 'method' => 'post' , 'id' => 'contactCreateForm'])!!}
    {{Form::token()}}
    <div class="form-group">
        {{Form::label('first_name' , 'First Name:')}}
        {{Form::text('first_name' , null , ['class' =>'form-control' , 'id' => 'first_name' , 'minlength' => 2 , 'maxlength' => 20])}}
    </div>

    <div class="form-group">
        {{Form::label('last_name' , 'Last Name:')}}
        {{Form::text('last_name' , null , ['class' =>'form-control' , 'id' => 'last_name' , 'minlength' => 2 , 'maxlength' => 20 ])}}
    </div>

    <div class="form-group">
        {{Form::label('email' , 'Email:')}}
        {{Form::email('email' , null , ['class' =>'form-control' , 'id' => 'email' , 'email' => 'true'])}}
    </div>

    <div class="form-group">
        {{Form::label('city' , 'City:')}}
        {{Form::text('city' , null , ['class' =>'form-control' , 'id' => 'city'])}}
    </div>

    <div class="form-group">
        {{Form::label('country' , 'Country:')}}
        {{Form::text('country' , null , ['class' =>'form-control' , 'id' => 'country'])}}
    </div>

    <div class="form-group">
        {{Form::label('job_title' , 'Job Title:')}}
        {{Form::text('job_title' , null , ['class' =>'form-control' , 'id' => 'job_title'])}}
    </div>

    <div class="form-group">
        {{Form::submit('Add Contact' , ['class' =>'btn btn-primary'])}}
    </div>
    {!!Form::close()!!}

  </div>
</div>
</div>
@endsection
