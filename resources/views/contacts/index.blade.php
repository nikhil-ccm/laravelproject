@extends('base')

@section('main')
<div class="col-sm-12">

    @if(session()->get('success'))
      <div class="alert alert-success">
        {{ session()->get('success') }}  
      </div>
    @endif
  </div>
    
 
    
<div class="row">
<div class="col-sm-12">
    <h1 class="display-3">Contacts</h1> 
    <div>
        <a style="margin: 19px;" href="{{ route('contacts.create')}}" class="btn btn-primary">New contact</a>
    </div>

    {{ Breadcrumbs::render('contacts') }}

  <table id="myTable" class="display">
    <thead>
        <tr>
          <td>ID</td>
          <td>Name</td>
          <td>Email</td>
          <td>Job Title</td>
          <td>City</td>
          <td>Country</td>
          <td>Actions</td>
        </tr>
    </thead>
    <tbody>
      @foreach($contacts as $contact)
      <tr>
          <td>{{$contact->id}}</td>
          <td>{{$contact->first_name}} {{$contact->last_name}}</td>
          <td>{{$contact->email}}</td>
          <td>{{$contact->job_title}}</td>
          <td>{{$contact->city}}</td>
          <td>{{$contact->country}}</td>
          <td>
              <a href="{{ route('contacts.edit',$contact->id)}}" class="btn btn-primary btn-sm">Edit</a>

            <button onclick="deleteData({{$contact->id}})" class="btn btn-danger btn-sm">Delete</button> 
            <!--<form action="{{ route('contacts.destroy', $contact->id)}}" method="post">
                @csrf
                @method('DELETE')
                <button class="btn btn-danger" type="submit">Delete</button>
              </form>-->
          </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  <script>
        $(document).ready( function () {
        var table = $('#myTable').DataTable();
      });
    </script>
    <script>

 function deleteData(id) {
  var csrf_token = $('meta[name="csrf-token"]').attr('content');
 swal({
   title : "are you sure?" , 
   text : "once deleted , you will not be able to recover data again?",
   icon : "warning",
   buttons : true,
   dangerMode: true,
 }).then((willDelete) => {
   if(willDelete) {
     $.ajax({
       url : "{{ url('contacts')}}" + '/' + id,
       type : "POST",
       data : {'_method' : 'DELETE', '_token' : csrf_token},
       success : function(data) {
        table.ajax.reload();
         swal({
           title : "delete done",
           text : "you clicked the button",
           icon : "success",
           button :"done"
         })
       },
       error : function() {
         swal({
           title : 'opps',
           text : 'something happened wrong',
           type : 'error',
           timer : '1500'
         })
       }
     });
   } else {
     swal('your file is safe!');
   }
 });
} 
</script>

@endsection